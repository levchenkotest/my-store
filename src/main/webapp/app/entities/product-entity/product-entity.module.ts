import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ProductEntityComponent } from './list/product-entity.component';
import { ProductEntityDetailComponent } from './detail/product-entity-detail.component';
import { ProductEntityUpdateComponent } from './update/product-entity-update.component';
import { ProductEntityDeleteDialogComponent } from './delete/product-entity-delete-dialog.component';
import { ProductEntityRoutingModule } from './route/product-entity-routing.module';

@NgModule({
  imports: [SharedModule, ProductEntityRoutingModule],
  declarations: [ProductEntityComponent, ProductEntityDetailComponent, ProductEntityUpdateComponent, ProductEntityDeleteDialogComponent],
})
export class ProductEntityModule {}
