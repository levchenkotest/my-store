import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IProductEntity } from '../product-entity.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../product-entity.test-samples';

import { ProductEntityService } from './product-entity.service';

const requireRestSample: IProductEntity = {
  ...sampleWithRequiredData,
};

describe('ProductEntity Service', () => {
  let service: ProductEntityService;
  let httpMock: HttpTestingController;
  let expectedResult: IProductEntity | IProductEntity[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ProductEntityService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a ProductEntity', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const productEntity = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(productEntity).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ProductEntity', () => {
      const productEntity = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(productEntity).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ProductEntity', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ProductEntity', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a ProductEntity', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addProductEntityToCollectionIfMissing', () => {
      it('should add a ProductEntity to an empty array', () => {
        const productEntity: IProductEntity = sampleWithRequiredData;
        expectedResult = service.addProductEntityToCollectionIfMissing([], productEntity);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(productEntity);
      });

      it('should not add a ProductEntity to an array that contains it', () => {
        const productEntity: IProductEntity = sampleWithRequiredData;
        const productEntityCollection: IProductEntity[] = [
          {
            ...productEntity,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addProductEntityToCollectionIfMissing(productEntityCollection, productEntity);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ProductEntity to an array that doesn't contain it", () => {
        const productEntity: IProductEntity = sampleWithRequiredData;
        const productEntityCollection: IProductEntity[] = [sampleWithPartialData];
        expectedResult = service.addProductEntityToCollectionIfMissing(productEntityCollection, productEntity);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(productEntity);
      });

      it('should add only unique ProductEntity to an array', () => {
        const productEntityArray: IProductEntity[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const productEntityCollection: IProductEntity[] = [sampleWithRequiredData];
        expectedResult = service.addProductEntityToCollectionIfMissing(productEntityCollection, ...productEntityArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const productEntity: IProductEntity = sampleWithRequiredData;
        const productEntity2: IProductEntity = sampleWithPartialData;
        expectedResult = service.addProductEntityToCollectionIfMissing([], productEntity, productEntity2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(productEntity);
        expect(expectedResult).toContain(productEntity2);
      });

      it('should accept null and undefined values', () => {
        const productEntity: IProductEntity = sampleWithRequiredData;
        expectedResult = service.addProductEntityToCollectionIfMissing([], null, productEntity, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(productEntity);
      });

      it('should return initial array if no ProductEntity is added', () => {
        const productEntityCollection: IProductEntity[] = [sampleWithRequiredData];
        expectedResult = service.addProductEntityToCollectionIfMissing(productEntityCollection, undefined, null);
        expect(expectedResult).toEqual(productEntityCollection);
      });
    });

    describe('compareProductEntity', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareProductEntity(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareProductEntity(entity1, entity2);
        const compareResult2 = service.compareProductEntity(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareProductEntity(entity1, entity2);
        const compareResult2 = service.compareProductEntity(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareProductEntity(entity1, entity2);
        const compareResult2 = service.compareProductEntity(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
