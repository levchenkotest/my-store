import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IProductEntity, NewProductEntity } from '../product-entity.model';

export type PartialUpdateProductEntity = Partial<IProductEntity> & Pick<IProductEntity, 'id'>;

export type EntityResponseType = HttpResponse<IProductEntity>;
export type EntityArrayResponseType = HttpResponse<IProductEntity[]>;

@Injectable({ providedIn: 'root' })
export class ProductEntityService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/product-entities');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(productEntity: NewProductEntity): Observable<EntityResponseType> {
    return this.http.post<IProductEntity>(this.resourceUrl, productEntity, { observe: 'response' });
  }

  update(productEntity: IProductEntity): Observable<EntityResponseType> {
    return this.http.put<IProductEntity>(`${this.resourceUrl}/${this.getProductEntityIdentifier(productEntity)}`, productEntity, {
      observe: 'response',
    });
  }

  partialUpdate(productEntity: PartialUpdateProductEntity): Observable<EntityResponseType> {
    return this.http.patch<IProductEntity>(`${this.resourceUrl}/${this.getProductEntityIdentifier(productEntity)}`, productEntity, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProductEntity>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IProductEntity[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getProductEntityIdentifier(productEntity: Pick<IProductEntity, 'id'>): number {
    return productEntity.id;
  }

  compareProductEntity(o1: Pick<IProductEntity, 'id'> | null, o2: Pick<IProductEntity, 'id'> | null): boolean {
    return o1 && o2 ? this.getProductEntityIdentifier(o1) === this.getProductEntityIdentifier(o2) : o1 === o2;
  }

  addProductEntityToCollectionIfMissing<Type extends Pick<IProductEntity, 'id'>>(
    productEntityCollection: Type[],
    ...productEntitiesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const productEntities: Type[] = productEntitiesToCheck.filter(isPresent);
    if (productEntities.length > 0) {
      const productEntityCollectionIdentifiers = productEntityCollection.map(
        productEntityItem => this.getProductEntityIdentifier(productEntityItem)!
      );
      const productEntitiesToAdd = productEntities.filter(productEntityItem => {
        const productEntityIdentifier = this.getProductEntityIdentifier(productEntityItem);
        if (productEntityCollectionIdentifiers.includes(productEntityIdentifier)) {
          return false;
        }
        productEntityCollectionIdentifiers.push(productEntityIdentifier);
        return true;
      });
      return [...productEntitiesToAdd, ...productEntityCollection];
    }
    return productEntityCollection;
  }
}
