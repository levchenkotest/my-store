import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../product-entity.test-samples';

import { ProductEntityFormService } from './product-entity-form.service';

describe('ProductEntity Form Service', () => {
  let service: ProductEntityFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProductEntityFormService);
  });

  describe('Service methods', () => {
    describe('createProductEntityFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createProductEntityFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            description: expect.any(Object),
            price: expect.any(Object),
            productSize: expect.any(Object),
            image: expect.any(Object),
            productCategory: expect.any(Object),
          })
        );
      });

      it('passing IProductEntity should create a new form with FormGroup', () => {
        const formGroup = service.createProductEntityFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            name: expect.any(Object),
            description: expect.any(Object),
            price: expect.any(Object),
            productSize: expect.any(Object),
            image: expect.any(Object),
            productCategory: expect.any(Object),
          })
        );
      });
    });

    describe('getProductEntity', () => {
      it('should return NewProductEntity for default ProductEntity initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createProductEntityFormGroup(sampleWithNewData);

        const productEntity = service.getProductEntity(formGroup) as any;

        expect(productEntity).toMatchObject(sampleWithNewData);
      });

      it('should return NewProductEntity for empty ProductEntity initial value', () => {
        const formGroup = service.createProductEntityFormGroup();

        const productEntity = service.getProductEntity(formGroup) as any;

        expect(productEntity).toMatchObject({});
      });

      it('should return IProductEntity', () => {
        const formGroup = service.createProductEntityFormGroup(sampleWithRequiredData);

        const productEntity = service.getProductEntity(formGroup) as any;

        expect(productEntity).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IProductEntity should not enable id FormControl', () => {
        const formGroup = service.createProductEntityFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewProductEntity should disable id FormControl', () => {
        const formGroup = service.createProductEntityFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
