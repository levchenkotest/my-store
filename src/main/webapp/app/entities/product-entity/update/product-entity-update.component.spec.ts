import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ProductEntityFormService } from './product-entity-form.service';
import { ProductEntityService } from '../service/product-entity.service';
import { IProductEntity } from '../product-entity.model';
import { IProductCategory } from 'app/entities/product-category/product-category.model';
import { ProductCategoryService } from 'app/entities/product-category/service/product-category.service';

import { ProductEntityUpdateComponent } from './product-entity-update.component';

describe('ProductEntity Management Update Component', () => {
  let comp: ProductEntityUpdateComponent;
  let fixture: ComponentFixture<ProductEntityUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let productEntityFormService: ProductEntityFormService;
  let productEntityService: ProductEntityService;
  let productCategoryService: ProductCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ProductEntityUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ProductEntityUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProductEntityUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    productEntityFormService = TestBed.inject(ProductEntityFormService);
    productEntityService = TestBed.inject(ProductEntityService);
    productCategoryService = TestBed.inject(ProductCategoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call ProductCategory query and add missing value', () => {
      const productEntity: IProductEntity = { id: 456 };
      const productCategory: IProductCategory = { id: 68501 };
      productEntity.productCategory = productCategory;

      const productCategoryCollection: IProductCategory[] = [{ id: 1777 }];
      jest.spyOn(productCategoryService, 'query').mockReturnValue(of(new HttpResponse({ body: productCategoryCollection })));
      const additionalProductCategories = [productCategory];
      const expectedCollection: IProductCategory[] = [...additionalProductCategories, ...productCategoryCollection];
      jest.spyOn(productCategoryService, 'addProductCategoryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ productEntity });
      comp.ngOnInit();

      expect(productCategoryService.query).toHaveBeenCalled();
      expect(productCategoryService.addProductCategoryToCollectionIfMissing).toHaveBeenCalledWith(
        productCategoryCollection,
        ...additionalProductCategories.map(expect.objectContaining)
      );
      expect(comp.productCategoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const productEntity: IProductEntity = { id: 456 };
      const productCategory: IProductCategory = { id: 72457 };
      productEntity.productCategory = productCategory;

      activatedRoute.data = of({ productEntity });
      comp.ngOnInit();

      expect(comp.productCategoriesSharedCollection).toContain(productCategory);
      expect(comp.productEntity).toEqual(productEntity);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IProductEntity>>();
      const productEntity = { id: 123 };
      jest.spyOn(productEntityFormService, 'getProductEntity').mockReturnValue(productEntity);
      jest.spyOn(productEntityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ productEntity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: productEntity }));
      saveSubject.complete();

      // THEN
      expect(productEntityFormService.getProductEntity).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(productEntityService.update).toHaveBeenCalledWith(expect.objectContaining(productEntity));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IProductEntity>>();
      const productEntity = { id: 123 };
      jest.spyOn(productEntityFormService, 'getProductEntity').mockReturnValue({ id: null });
      jest.spyOn(productEntityService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ productEntity: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: productEntity }));
      saveSubject.complete();

      // THEN
      expect(productEntityFormService.getProductEntity).toHaveBeenCalled();
      expect(productEntityService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IProductEntity>>();
      const productEntity = { id: 123 };
      jest.spyOn(productEntityService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ productEntity });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(productEntityService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareProductCategory', () => {
      it('Should forward to productCategoryService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(productCategoryService, 'compareProductCategory');
        comp.compareProductCategory(entity, entity2);
        expect(productCategoryService.compareProductCategory).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
