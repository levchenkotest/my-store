import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IProductEntity, NewProductEntity } from '../product-entity.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IProductEntity for edit and NewProductEntityFormGroupInput for create.
 */
type ProductEntityFormGroupInput = IProductEntity | PartialWithRequiredKeyOf<NewProductEntity>;

type ProductEntityFormDefaults = Pick<NewProductEntity, 'id'>;

type ProductEntityFormGroupContent = {
  id: FormControl<IProductEntity['id'] | NewProductEntity['id']>;
  name: FormControl<IProductEntity['name']>;
  description: FormControl<IProductEntity['description']>;
  price: FormControl<IProductEntity['price']>;
  productSize: FormControl<IProductEntity['productSize']>;
  image: FormControl<IProductEntity['image']>;
  imageContentType: FormControl<IProductEntity['imageContentType']>;
  productCategory: FormControl<IProductEntity['productCategory']>;
};

export type ProductEntityFormGroup = FormGroup<ProductEntityFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ProductEntityFormService {
  createProductEntityFormGroup(productEntity: ProductEntityFormGroupInput = { id: null }): ProductEntityFormGroup {
    const productEntityRawValue = {
      ...this.getFormDefaults(),
      ...productEntity,
    };
    return new FormGroup<ProductEntityFormGroupContent>({
      id: new FormControl(
        { value: productEntityRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      name: new FormControl(productEntityRawValue.name, {
        validators: [Validators.required],
      }),
      description: new FormControl(productEntityRawValue.description),
      price: new FormControl(productEntityRawValue.price, {
        validators: [Validators.required, Validators.min(0)],
      }),
      productSize: new FormControl(productEntityRawValue.productSize, {
        validators: [Validators.required],
      }),
      image: new FormControl(productEntityRawValue.image),
      imageContentType: new FormControl(productEntityRawValue.imageContentType),
      productCategory: new FormControl(productEntityRawValue.productCategory),
    });
  }

  getProductEntity(form: ProductEntityFormGroup): IProductEntity | NewProductEntity {
    return form.getRawValue() as IProductEntity | NewProductEntity;
  }

  resetForm(form: ProductEntityFormGroup, productEntity: ProductEntityFormGroupInput): void {
    const productEntityRawValue = { ...this.getFormDefaults(), ...productEntity };
    form.reset(
      {
        ...productEntityRawValue,
        id: { value: productEntityRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ProductEntityFormDefaults {
    return {
      id: null,
    };
  }
}
