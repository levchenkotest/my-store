import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ProductEntityComponent } from '../list/product-entity.component';
import { ProductEntityDetailComponent } from '../detail/product-entity-detail.component';
import { ProductEntityUpdateComponent } from '../update/product-entity-update.component';
import { ProductEntityRoutingResolveService } from './product-entity-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const productEntityRoute: Routes = [
  {
    path: '',
    component: ProductEntityComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ProductEntityDetailComponent,
    resolve: {
      productEntity: ProductEntityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ProductEntityUpdateComponent,
    resolve: {
      productEntity: ProductEntityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ProductEntityUpdateComponent,
    resolve: {
      productEntity: ProductEntityRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(productEntityRoute)],
  exports: [RouterModule],
})
export class ProductEntityRoutingModule {}
