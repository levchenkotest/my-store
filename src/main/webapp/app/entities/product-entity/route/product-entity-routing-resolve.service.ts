import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IProductEntity } from '../product-entity.model';
import { ProductEntityService } from '../service/product-entity.service';

@Injectable({ providedIn: 'root' })
export class ProductEntityRoutingResolveService implements Resolve<IProductEntity | null> {
  constructor(protected service: ProductEntityService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IProductEntity | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((productEntity: HttpResponse<IProductEntity>) => {
          if (productEntity.body) {
            return of(productEntity.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}
