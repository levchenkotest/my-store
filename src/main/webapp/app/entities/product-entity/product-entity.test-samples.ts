import { Size } from 'app/entities/enumerations/size.model';

import { IProductEntity, NewProductEntity } from './product-entity.model';

export const sampleWithRequiredData: IProductEntity = {
  id: 4419,
  name: 'Gourde AGP Mandatory',
  price: 11747,
  productSize: Size['XL'],
};

export const sampleWithPartialData: IProductEntity = {
  id: 66331,
  name: 'Outdoors Refined',
  description: 'bluetooth functionalities',
  price: 51181,
  productSize: Size['L'],
  image: '../fake-data/blob/hipster.png',
  imageContentType: 'unknown',
};

export const sampleWithFullData: IProductEntity = {
  id: 96998,
  name: 'olive Stream',
  description: 'SMTP enable monetize',
  price: 60908,
  productSize: Size['S'],
  image: '../fake-data/blob/hipster.png',
  imageContentType: 'unknown',
};

export const sampleWithNewData: NewProductEntity = {
  name: 'hacking synthesizing Walks',
  price: 66900,
  productSize: Size['L'],
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
