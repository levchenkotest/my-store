import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ProductEntityService } from '../service/product-entity.service';

import { ProductEntityComponent } from './product-entity.component';

describe('ProductEntity Management Component', () => {
  let comp: ProductEntityComponent;
  let fixture: ComponentFixture<ProductEntityComponent>;
  let service: ProductEntityService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'product-entity', component: ProductEntityComponent }]), HttpClientTestingModule],
      declarations: [ProductEntityComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(ProductEntityComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ProductEntityComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ProductEntityService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.productEntities?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to productEntityService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getProductEntityIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getProductEntityIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
