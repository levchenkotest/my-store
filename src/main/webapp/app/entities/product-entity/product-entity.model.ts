import { IProductCategory } from 'app/entities/product-category/product-category.model';
import { Size } from 'app/entities/enumerations/size.model';

export interface IProductEntity {
  id: number;
  name?: string | null;
  description?: string | null;
  price?: number | null;
  productSize?: Size | null;
  image?: string | null;
  imageContentType?: string | null;
  productCategory?: Pick<IProductCategory, 'id' | 'name'> | null;
}

export type NewProductEntity = Omit<IProductEntity, 'id'> & { id: null };
