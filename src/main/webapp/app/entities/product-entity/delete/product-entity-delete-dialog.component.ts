import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IProductEntity } from '../product-entity.model';
import { ProductEntityService } from '../service/product-entity.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './product-entity-delete-dialog.component.html',
})
export class ProductEntityDeleteDialogComponent {
  productEntity?: IProductEntity;

  constructor(protected productEntityService: ProductEntityService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.productEntityService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}
