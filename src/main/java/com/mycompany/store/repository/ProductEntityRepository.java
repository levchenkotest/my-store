package com.mycompany.store.repository;

import com.mycompany.store.domain.ProductEntity;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ProductEntity entity.
 */
@Repository
public interface ProductEntityRepository extends JpaRepository<ProductEntity, Long> {
    default Optional<ProductEntity> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<ProductEntity> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<ProductEntity> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct productEntity from ProductEntity productEntity left join fetch productEntity.productCategory",
        countQuery = "select count(distinct productEntity) from ProductEntity productEntity"
    )
    Page<ProductEntity> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct productEntity from ProductEntity productEntity left join fetch productEntity.productCategory")
    List<ProductEntity> findAllWithToOneRelationships();

    @Query(
        "select productEntity from ProductEntity productEntity left join fetch productEntity.productCategory where productEntity.id =:id"
    )
    Optional<ProductEntity> findOneWithToOneRelationships(@Param("id") Long id);
}
