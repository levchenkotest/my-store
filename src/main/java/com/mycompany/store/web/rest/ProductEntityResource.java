package com.mycompany.store.web.rest;

import com.mycompany.store.domain.ProductEntity;
import com.mycompany.store.repository.ProductEntityRepository;
import com.mycompany.store.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.store.domain.ProductEntity}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ProductEntityResource {

    private final Logger log = LoggerFactory.getLogger(ProductEntityResource.class);

    private static final String ENTITY_NAME = "productEntity";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductEntityRepository productEntityRepository;

    public ProductEntityResource(ProductEntityRepository productEntityRepository) {
        this.productEntityRepository = productEntityRepository;
    }

    /**
     * {@code POST  /product-entities} : Create a new productEntity.
     *
     * @param productEntity the productEntity to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productEntity, or with status {@code 400 (Bad Request)} if the productEntity has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-entities")
    public ResponseEntity<ProductEntity> createProductEntity(@Valid @RequestBody ProductEntity productEntity) throws URISyntaxException {
        log.debug("REST request to save ProductEntity : {}", productEntity);
        if (productEntity.getId() != null) {
            throw new BadRequestAlertException("A new productEntity cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductEntity result = productEntityRepository.save(productEntity);
        return ResponseEntity
            .created(new URI("/api/product-entities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-entities/:id} : Updates an existing productEntity.
     *
     * @param id the id of the productEntity to save.
     * @param productEntity the productEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productEntity,
     * or with status {@code 400 (Bad Request)} if the productEntity is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-entities/{id}")
    public ResponseEntity<ProductEntity> updateProductEntity(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ProductEntity productEntity
    ) throws URISyntaxException {
        log.debug("REST request to update ProductEntity : {}, {}", id, productEntity);
        if (productEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, productEntity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!productEntityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ProductEntity result = productEntityRepository.save(productEntity);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productEntity.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /product-entities/:id} : Partial updates given fields of an existing productEntity, field will ignore if it is null
     *
     * @param id the id of the productEntity to save.
     * @param productEntity the productEntity to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productEntity,
     * or with status {@code 400 (Bad Request)} if the productEntity is not valid,
     * or with status {@code 404 (Not Found)} if the productEntity is not found,
     * or with status {@code 500 (Internal Server Error)} if the productEntity couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/product-entities/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ProductEntity> partialUpdateProductEntity(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ProductEntity productEntity
    ) throws URISyntaxException {
        log.debug("REST request to partial update ProductEntity partially : {}, {}", id, productEntity);
        if (productEntity.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, productEntity.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!productEntityRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ProductEntity> result = productEntityRepository
            .findById(productEntity.getId())
            .map(existingProductEntity -> {
                if (productEntity.getName() != null) {
                    existingProductEntity.setName(productEntity.getName());
                }
                if (productEntity.getDescription() != null) {
                    existingProductEntity.setDescription(productEntity.getDescription());
                }
                if (productEntity.getPrice() != null) {
                    existingProductEntity.setPrice(productEntity.getPrice());
                }
                if (productEntity.getProductSize() != null) {
                    existingProductEntity.setProductSize(productEntity.getProductSize());
                }
                if (productEntity.getImage() != null) {
                    existingProductEntity.setImage(productEntity.getImage());
                }
                if (productEntity.getImageContentType() != null) {
                    existingProductEntity.setImageContentType(productEntity.getImageContentType());
                }

                return existingProductEntity;
            })
            .map(productEntityRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productEntity.getId().toString())
        );
    }

    /**
     * {@code GET  /product-entities} : get all the productEntities.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productEntities in body.
     */
    @GetMapping("/product-entities")
    public List<ProductEntity> getAllProductEntities(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all ProductEntities");
        if (eagerload) {
            return productEntityRepository.findAllWithEagerRelationships();
        } else {
            return productEntityRepository.findAll();
        }
    }

    /**
     * {@code GET  /product-entities/:id} : get the "id" productEntity.
     *
     * @param id the id of the productEntity to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productEntity, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-entities/{id}")
    public ResponseEntity<ProductEntity> getProductEntity(@PathVariable Long id) {
        log.debug("REST request to get ProductEntity : {}", id);
        Optional<ProductEntity> productEntity = productEntityRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(productEntity);
    }

    /**
     * {@code DELETE  /product-entities/:id} : delete the "id" productEntity.
     *
     * @param id the id of the productEntity to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-entities/{id}")
    public ResponseEntity<Void> deleteProductEntity(@PathVariable Long id) {
        log.debug("REST request to delete ProductEntity : {}", id);
        productEntityRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
