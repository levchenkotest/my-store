package com.mycompany.store.web.rest;

import static com.mycompany.store.web.rest.TestUtil.sameNumber;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.mycompany.store.IntegrationTest;
import com.mycompany.store.domain.ProductEntity;
import com.mycompany.store.domain.enumeration.Size;
import com.mycompany.store.repository.ProductEntityRepository;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link ProductEntityResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ProductEntityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_PRICE = new BigDecimal(0);
    private static final BigDecimal UPDATED_PRICE = new BigDecimal(1);

    private static final Size DEFAULT_PRODUCT_SIZE = Size.S;
    private static final Size UPDATED_PRODUCT_SIZE = Size.M;

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    private static final String ENTITY_API_URL = "/api/product-entities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ProductEntityRepository productEntityRepository;

    @Mock
    private ProductEntityRepository productEntityRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductEntityMockMvc;

    private ProductEntity productEntity;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductEntity createEntity(EntityManager em) {
        ProductEntity productEntity = new ProductEntity()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .price(DEFAULT_PRICE)
            .productSize(DEFAULT_PRODUCT_SIZE)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE);
        return productEntity;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductEntity createUpdatedEntity(EntityManager em) {
        ProductEntity productEntity = new ProductEntity()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .price(UPDATED_PRICE)
            .productSize(UPDATED_PRODUCT_SIZE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);
        return productEntity;
    }

    @BeforeEach
    public void initTest() {
        productEntity = createEntity(em);
    }

    @Test
    @Transactional
    void createProductEntity() throws Exception {
        int databaseSizeBeforeCreate = productEntityRepository.findAll().size();
        // Create the ProductEntity
        restProductEntityMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isCreated());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeCreate + 1);
        ProductEntity testProductEntity = productEntityList.get(productEntityList.size() - 1);
        assertThat(testProductEntity.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testProductEntity.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testProductEntity.getPrice()).isEqualByComparingTo(DEFAULT_PRICE);
        assertThat(testProductEntity.getProductSize()).isEqualTo(DEFAULT_PRODUCT_SIZE);
        assertThat(testProductEntity.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testProductEntity.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void createProductEntityWithExistingId() throws Exception {
        // Create the ProductEntity with an existing ID
        productEntity.setId(1L);

        int databaseSizeBeforeCreate = productEntityRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductEntityMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = productEntityRepository.findAll().size();
        // set the field null
        productEntity.setName(null);

        // Create the ProductEntity, which fails.

        restProductEntityMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isBadRequest());

        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPriceIsRequired() throws Exception {
        int databaseSizeBeforeTest = productEntityRepository.findAll().size();
        // set the field null
        productEntity.setPrice(null);

        // Create the ProductEntity, which fails.

        restProductEntityMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isBadRequest());

        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkProductSizeIsRequired() throws Exception {
        int databaseSizeBeforeTest = productEntityRepository.findAll().size();
        // set the field null
        productEntity.setProductSize(null);

        // Create the ProductEntity, which fails.

        restProductEntityMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isBadRequest());

        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllProductEntities() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        // Get all the productEntityList
        restProductEntityMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productEntity.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].price").value(hasItem(sameNumber(DEFAULT_PRICE))))
            .andExpect(jsonPath("$.[*].productSize").value(hasItem(DEFAULT_PRODUCT_SIZE.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProductEntitiesWithEagerRelationshipsIsEnabled() throws Exception {
        when(productEntityRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProductEntityMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(productEntityRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProductEntitiesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(productEntityRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProductEntityMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(productEntityRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getProductEntity() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        // Get the productEntity
        restProductEntityMockMvc
            .perform(get(ENTITY_API_URL_ID, productEntity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productEntity.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.price").value(sameNumber(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.productSize").value(DEFAULT_PRODUCT_SIZE.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)));
    }

    @Test
    @Transactional
    void getNonExistingProductEntity() throws Exception {
        // Get the productEntity
        restProductEntityMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingProductEntity() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();

        // Update the productEntity
        ProductEntity updatedProductEntity = productEntityRepository.findById(productEntity.getId()).get();
        // Disconnect from session so that the updates on updatedProductEntity are not directly saved in db
        em.detach(updatedProductEntity);
        updatedProductEntity
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .price(UPDATED_PRICE)
            .productSize(UPDATED_PRODUCT_SIZE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);

        restProductEntityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedProductEntity.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedProductEntity))
            )
            .andExpect(status().isOk());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
        ProductEntity testProductEntity = productEntityList.get(productEntityList.size() - 1);
        assertThat(testProductEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProductEntity.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProductEntity.getPrice()).isEqualByComparingTo(UPDATED_PRICE);
        assertThat(testProductEntity.getProductSize()).isEqualTo(UPDATED_PRODUCT_SIZE);
        assertThat(testProductEntity.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testProductEntity.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void putNonExistingProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();
        productEntity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductEntityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, productEntity.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productEntity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();
        productEntity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductEntityMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(productEntity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();
        productEntity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductEntityMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(productEntity)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProductEntityWithPatch() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();

        // Update the productEntity using partial update
        ProductEntity partialUpdatedProductEntity = new ProductEntity();
        partialUpdatedProductEntity.setId(productEntity.getId());

        partialUpdatedProductEntity.name(UPDATED_NAME).description(UPDATED_DESCRIPTION);

        restProductEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductEntity.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProductEntity))
            )
            .andExpect(status().isOk());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
        ProductEntity testProductEntity = productEntityList.get(productEntityList.size() - 1);
        assertThat(testProductEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProductEntity.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProductEntity.getPrice()).isEqualByComparingTo(DEFAULT_PRICE);
        assertThat(testProductEntity.getProductSize()).isEqualTo(DEFAULT_PRODUCT_SIZE);
        assertThat(testProductEntity.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testProductEntity.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void fullUpdateProductEntityWithPatch() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();

        // Update the productEntity using partial update
        ProductEntity partialUpdatedProductEntity = new ProductEntity();
        partialUpdatedProductEntity.setId(productEntity.getId());

        partialUpdatedProductEntity
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .price(UPDATED_PRICE)
            .productSize(UPDATED_PRODUCT_SIZE)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);

        restProductEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProductEntity.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProductEntity))
            )
            .andExpect(status().isOk());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
        ProductEntity testProductEntity = productEntityList.get(productEntityList.size() - 1);
        assertThat(testProductEntity.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testProductEntity.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testProductEntity.getPrice()).isEqualByComparingTo(UPDATED_PRICE);
        assertThat(testProductEntity.getProductSize()).isEqualTo(UPDATED_PRODUCT_SIZE);
        assertThat(testProductEntity.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testProductEntity.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    void patchNonExistingProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();
        productEntity.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, productEntity.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productEntity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();
        productEntity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductEntityMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(productEntity))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProductEntity() throws Exception {
        int databaseSizeBeforeUpdate = productEntityRepository.findAll().size();
        productEntity.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProductEntityMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(productEntity))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProductEntity in the database
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProductEntity() throws Exception {
        // Initialize the database
        productEntityRepository.saveAndFlush(productEntity);

        int databaseSizeBeforeDelete = productEntityRepository.findAll().size();

        // Delete the productEntity
        restProductEntityMockMvc
            .perform(delete(ENTITY_API_URL_ID, productEntity.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductEntity> productEntityList = productEntityRepository.findAll();
        assertThat(productEntityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
